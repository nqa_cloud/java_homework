public interface Behaviour {
    public abstract void Voice();
    public abstract void Eat();
    public abstract void Run();
}
