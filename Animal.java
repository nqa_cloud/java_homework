import java.util.Scanner;

public abstract class Animal {
    private String name;
    private int size;
    private boolean Owner;

    //решила сделать в качестве булеан и позволить задавать адрес вручную, в зависимости от "степени дикости"
    Boolean getOwner() {
        return Owner;
    }
    boolean setOwner(boolean b) {
        if(b==true){
            System.out.println("domesticated");
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите адрес:");
            String adress = scan.nextLine();
            System.out.println(adress);
        } else {System.out.println("stray");}
        return b;
    }

    String getName() {
        return name;
    }

    void setName(String _name) {
        this.name = _name;
        System.out.println("животину зовут:" + _name);
    }

    public void setSize(int sz) {
        if(sz>=1) {
            this.size=sz;
        }
    }
    public int getSize() {
        return size;
    }
}
