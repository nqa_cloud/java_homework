public class Test {
    public static void main(String[] args) {
        Cat Murka = new Cat();
        Cat Barsik = new Cat();
        Dog noname = new Dog();
        Dog Bolto = new Dog();

        Murka.setName("Мурка");
        Murka.setSize(13);
        Murka.Eat();
        Murka.Run();
        Murka.Voice();

        Barsik.setName("Барсик");

        Barsik.setSize(6);
        Barsik.Eat();
        Barsik.Run();
        Barsik.Voice();

        noname.setSize(67);
        noname.setOwner(true);
        noname.Run();
        noname.Eat();
        noname.Voice();

        Bolto.setName("Болто " + "это очень гордое имя");
        Bolto.setOwner(false);
        Bolto.setSize(26);
        Bolto.Run();
        Bolto.Voice();
        Bolto.Eat();
    }
}

